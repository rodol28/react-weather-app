import React from 'react';
    
const WeatherForm = props => (
    <div className="card card-body">
        <form onSubmit={props.getWeather}>
            <div className="form-group">
                <input
                    type="text"
                    name="city"
                    placeholder="Your city name"
                    className="form-control"
                    autoFocus
                />
            </div>
            <div className="form-group">
                <input
                    type="text"
                    name="country"
                    placeholder="Your country name"
                    className="form-control"
                />
            </div>
            <div className="form-group">
                <button className="btn btn-success btn-block form-control">
                    Get weather
                </button>
            </div>
        </form>
    </div>
);


export default WeatherForm;
